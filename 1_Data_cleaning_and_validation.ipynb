{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Brilliant Planet Data Visualisation Test Instructions\n",
    "\n",
    "This test is designed to see how you read, process and present a data file. \n",
    "\n",
    "You can carry out the test in your own time, but you should spend no more than 2-3 hours on the task before submitting your answers.\n",
    "\n",
    "Attached with these instructions is a data file containing weather data encoded in Apache Parquet format. Your task is to read in this file, perform some quality control and cleanup steps then provide some plots and analysis. \n",
    "\n",
    "There are a number of categories of tasks below, in order of priority. Within each category, tasks are provided roughly in order of difficulty. Tackle whatever you can in the time available in whatever mix you think is appropriate.\n",
    "\n",
    "### Data cleaning and validation:\n",
    "\n",
    "- Make sure you can read in the Parquet file properly using your chosen toolchain.\n",
    "- Some rows in the dataset contain blank entries, which may have been decoded as the `NaN` value.\n",
    "    - Add an option to remove these entries completely from the final dataset.\n",
    "    - Add the capability to report the number of rows that contain blank entries.\n",
    "- In some cases, sensor failure caused the same value to be erroneously repeated many times.\n",
    "    - Create a configurable warning that reports when a value in a particular column is repeated more than N times.\n",
    "- Each row in the dataset has a timestamp. \n",
    "    - These are mostly contiguous, but there are also some gaps - report the number of gaps and their size.\n",
    "    - Report any instance where the same timestamp is used in more than one row.\n",
    "\n",
    "### Summary statistics:\n",
    "\n",
    "- Report the mean temperature and wind speed for the lifetime of the file.\n",
    "    - Ensure the value you report works for columns where `NaN`s are present\n",
    "- Allow the user to specify a date range and provide summary statistics over that time.\n",
    "- To check for outliers, allow an option to report on values from each column that are more than N standard deviations away from the mean\n",
    "\n",
    "### Plotting and visualisation:\n",
    "\n",
    "- A plot of the temperature and wind speed over the dates and times in the file.\n",
    "    - This could be a bar or line plot\n",
    "- Allow the user to specify a column from the CSV file and plot that\n",
    "- Allow the user to specify a date range and provide plots over that time\n",
    "\n",
    "### What tools can I use?\n",
    "\n",
    "You should your favourite programming language or analysis framework - for example, Python + Pandas + Matplotlib, to read in the CSV file and process it.\n",
    "\n",
    "\n",
    "### How should I deliver my results?\n",
    "\n",
    "You should return a zip bundle containing:\n",
    "\n",
    "- The source code/other files you have created to produce your visualisation and statistics\n",
    "- Instructions on how to run your code and reproduce the analysis\n",
    "\n",
    "### What else should I consider?\n",
    "\n",
    "- You need to make sure that however you package your code, it can be run by us to check your results. For example, if you are using Python, consider using a virtual environment / requirements.txt file to describe the execution environment.\n",
    "- In a real setting, this analysis would just be the beginning of working with this type of file. You should engineer your code to imagine how we might want to develop and extend it to other uses and more sophisticated analysis. This could include:\n",
    "    - Carefully packaging the code to read and proprocess the file\n",
    "    - Thinking about how you store the data after reading to make it easy to build analysis applications on top of this\n",
    "- There are several options for how you formulate your code to produce the desired output. \n",
    "    - Run the code once to produce all the outputs you support in separate files\n",
    "    - Have an option to specify what output should be produced on each run\n",
    "    - Produce a report that packages a collection of plots and other outputs together\n",
    "    - Create an interactive dashboard of the data"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# The Task\n",
    "### Data cleaning and validation:\n",
    "\n",
    "Firstly, the Parquet file must be read, which can be achieved by using the Pandas library. The dataset is read into the weather_data dataframe."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {},
   "outputs": [],
   "source": [
    "weather_data = pd.read_parquet('weatherlink_data_on_site_updated.parquet')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some rows in the dataset contain blank entries (many of which have been decoded as NaN values).\n",
    "The following code counts the number of blank or NaN values, and allows the user to remove these rows.\n",
    "\n",
    "A new dataframe is created in this process named \"weather_data_clean\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 65,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get the number of missing data points per column\n",
    "missing_values_count = weather_data.isnull().sum()\n",
    "\n",
    "# The user can run the following code to remove rows with NaN values, and reset the row index\n",
    "weather_data_clean=weather_data.dropna().reset_index(drop=True)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In some cases, sensor failure caused the same value to be erroneously repeated many times. A function is defined to check each column and reports a warning message if a value is repeated more than N times.\n",
    "The value of N can be changed as required."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 66,
   "metadata": {},
   "outputs": [],
   "source": [
    "def check_column_duplicates(dataframe, column_name, n):\n",
    "    \"\"\"\n",
    "    Checks if a value in a particular column is repeated more than N times.\n",
    "    Saves a warning message if any duplicates are found in \"Duplicate_values.txt\"\n",
    "    \"\"\"\n",
    "    duplicates = dataframe[column_name].value_counts()\n",
    "    duplicates = duplicates[duplicates > n]\n",
    "    \n",
    "    if not duplicates.empty:\n",
    "        warning_message = f\"Warning: {column_name} column has values that are repeated more than {n} times:\\n\"\n",
    "        for value, count in duplicates.items():\n",
    "            warning_message += f\"  - {value}: {count} times\\n\"\n",
    "        f = open(\"Duplicate_values.txt\", \"a\")\n",
    "        f.write(warning_message)\n",
    "        f.close()\n",
    "\n",
    "# Each sensor column title is stored in a list\n",
    "sensor_columns = list(weather_data_clean.columns)\n",
    "\n",
    "# A value for n is provided\n",
    "n = 1000\n",
    "\n",
    "# Create empty output file\n",
    "f = open(\"Duplicate_values.txt\", \"w\")\n",
    "f.close()\n",
    "\n",
    "# The function is then called for each sensor column\n",
    "for i in sensor_columns:\n",
    "       check_column_duplicates(weather_data_clean, i, n)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then report on the number of gaps using the 'DateTime' column"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 67,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Take the diff of the first column (drop 1st row since it's undefined)\n",
    "deltas = weather_data_clean['DateTime'].diff()[1:]\n",
    "\n",
    "# Find instances where gaps larger than n days occur and the size of these gaps\n",
    "n = 2\n",
    "gaps = deltas[deltas > timedelta(days=n)]\n",
    "\n",
    "# Save results to file\n",
    "f = open(\"Gaps.txt\", \"w\")\n",
    "f.write(f'There were {len(gaps)} gaps larger than {n} days')\n",
    "f.write(f\"{gaps} \\n \\n\")\n",
    "f.close()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And also report any instances where the same timestamp is used in more than one entry"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 68,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Check for duplicate timestamps\n",
    "duplicate_timestamps = weather_data_clean['DateTime'].duplicated()\n",
    "if any(duplicate_timestamps):\n",
    "    f = open(\"Gaps.txt\", \"a\")\n",
    "    f.write(\"\\n\")\n",
    "    f.write(f\"{duplicate_timestamps.sum()} Duplicate timestamps found:\")\n",
    "    f.write(f\"{weather_data_clean[duplicate_timestamps]}\")\n",
    "    f.close()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Brilliant_Planet",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.11"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
