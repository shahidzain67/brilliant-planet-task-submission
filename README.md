# Brilliant planet task submission

## Pre-requisites
All prerequisites are included in the file "req.txt"
This project has utilised python version 3.10.11


## Steps to install conda environment with all pre-requisites installed

### If Anaconda navigator is installed (easiest method)
- Open Anaconda navigator
- Click on Environments - Import
- Import the "environment.yml" file
- Select the "environment
- Click on Home and open jupyter notebook
- load the "Master.ipynb" file
- Click on kernal - Change kernal - firstEnv

### If Anaconda navigator is not installed 
1) Create the environment from the environment.yml file:

    conda env create -f environment.yml

The first line of the yml file sets the new environment's name.

2) Activate the new environment: 

    conda activate myenv

3) Verify that the new environment was installed correctly:

    conda env list
You can also use conda info --envs.


## Getting started

Install prerequisites on a python instance/virtual environment OR install the "environment.yml" environment as described above

Download all files

To run the task file, please open and run "Master.ipynb"
The file should be run in the same folder as all other task files submitted.

The user will be asked to input data at particular points in the file to produce the specified outputs.

## Notes
The task submission has been structred with the idea of making futher file and plot management repeatable and accessible.